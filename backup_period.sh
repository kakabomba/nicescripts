#!/bin/bash

bdir=/mnt

if [[ "$(date '+%j')" = "350" ]]; then
   atn=yearly
elif [[ "$(date '+%d')" = "01" ]]; then
   atn=monthly
elif [[ "$(date '+%u')" = "1" ]]; then
   atn=weekly
else
   arn=daily
fi

fname="$bdir/"$(date '+%F_%T')"_"$atn".sql.gz"

mount $bdir && PGPASSWORD="" pg_dump -h somehost -U pfuser profireader | gzip > $fname && find $bdir/ -type f -mtime +1 -name '*_'"$atn"'.sql.gz' -execdir rm -- '{}' \;

ls -l1sh $fname

umount $bdir

